# README : MY Profile service #


### What is this repository for? ###

* Restful service that return the JOSN-data of portfolio
* rest end-point : [GET]/portfolio
* view end-point : [GET]/portfolios

### How do I get set up locally? ###
* Clone the repository
* Start the java application
* Others[TBD]
* [Dependencies]
* [Database configuration]
* [How to run tests]
* [Deployment instructions]

### How do start locally? ###
* Run applicatoin from MyProfileServiceApplication 
* Start the java application
*endpoint- 
*[GET] localhost:8080/portfolio
*[GET] localhost:8080/portfolios for view page


### Technologies/Techniques Used-used ###
* Java-8
* Design Pattern - Builder Design Pattern
* Springboot
* TBD[JPA/H2-DB ]
* TBD[Caching]
* TBD[Spring Security on API]
* Spring security -JWT
* Fault tolerant - retry , readtimeout ,conencttimeout
### Deployment ###
* https://dashboard.heroku.com/apps/myprofileser-feature-ui-jwl8x7/settings


### Junits
* Writing tests
	* Positive senario
	* private method test
	* exception test

### Who do I talk to? ###

* Repo Owner: https://linkedln.com/in/sevanand-yadav


### ISSUES-RESOLUTION  ###
* Autowired searches for default constructor
* JSON serialization needs to have public getters/setters
* TODO: 
* [Get]/portfolio seems not idempotent - json size increasing in  every call from post man 
* jwt and test using postman
### H2 TROUBLESHOOT  ###
* Just enabled console from property another kept commented it showed
* JSON serialization needs to have public getters/setters
* 
### IntelliJ- shortcut keys  ###
* Format code - ctrl+alt+L
* Remove unused imports/organize - ctrl+alt++o
* 
