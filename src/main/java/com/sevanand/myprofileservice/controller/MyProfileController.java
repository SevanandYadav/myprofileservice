package com.sevanand.myprofileservice.controller;

import com.sevanand.myprofileservice.entity.Profile;
import com.sevanand.myprofileservice.entity.professional.ProfessionalDetails;
import com.sevanand.myprofileservice.service.MyProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyProfileController {

    @Autowired
    private MyProfileService myProfileService;

    @GetMapping("/portfolio")
    public Profile getProfile() {
        return myProfileService.getProfile();

    }
    @GetMapping("/profile/expr")
    public ProfessionalDetails getExperience() {

        return  myProfileService.getProfessionalDetails();

    }
    @PostMapping("/portfolio/experience")
    public String addExperience() {
        return "av";
    }

    @DeleteMapping("/portfolio/something")
    public String deleteHobby() {
        return "json Data";
    }

    @PutMapping("/portfolio/users")
    public String updateHobbies() {
        return "json Data hobbies";
    }
}
