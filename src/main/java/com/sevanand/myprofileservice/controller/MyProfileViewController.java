package com.sevanand.myprofileservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.sevanand.myprofileservice.entity.common.BasicDetails;
import com.sevanand.myprofileservice.service.MyProfileService;

@Controller
public class MyProfileViewController {

	@Autowired
	private MyProfileService myProfileService;

	@GetMapping("/portfolios")
	public String getProfile(Model model) {
		// return new MyProfileService().getProfile();
		setModelData(model);

		return "indexPage";

	}

	@GetMapping("/about")
	public String getAboutPage() {

		return "fragments/about";
	}

	@GetMapping("/blog")
	public String getBlogPage() {

		return "fragments/blog";
	}

	@GetMapping("/contact")
	public String getContactPage() {

		return "fragments/contact";
	}

	@GetMapping("/*")
	public String getProfileDefault(Model mode) {
		setModelData(mode);
		return "indexPage";
	}
	@GetMapping("/error")
	public String getProfileError(Model mode) {
		setModelData(mode);
		return "error";
	}

	private void setModelData(Model model) {
		BasicDetails basicDetails = myProfileService.getProfile().getBasicDetails();

		StringBuilder sb = new StringBuilder(basicDetails.getDob().getDay());
		sb.append("-");
		sb.append(basicDetails.getDob().getMonth());
		sb.append("-");
		sb.append(basicDetails.getDob().getYear());

		model.addAttribute("firstName", basicDetails.getFirstName());//
		model.addAttribute("lastName", basicDetails.getLastName());
		model.addAttribute("dob", String.valueOf(sb));
	}

}
