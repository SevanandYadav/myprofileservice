package com.sevanand.myprofileservice.service;

import com.sevanand.myprofileservice.dao.MyProfileDao;
import com.sevanand.myprofileservice.entity.Profile;
import com.sevanand.myprofileservice.entity.professional.ProfessionalDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyProfileService {
    @Autowired
    private MyProfileDao dao;

    public Profile getProfile() {

        return dao.getProfileDetails();
    }

    public ProfessionalDetails getProfessionalDetails() {

        return dao.getProfessionalDetails();
    }

}
