package com.sevanand.myprofileservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyProfileServiceApplicationTests {

	@Test
	void contextLoads() {
	}

	/*
	 * Junit 4 0 seems not using j4
	 * 
	 * @Test(expected=NullPointerException.class) public void runTimeExceptionTest()
	 * { null.length(); }
	 */

	/* Junit 5 */

	@Test
	public void runTimeExceptionTest() {
		String s = new String();
		Exception e = Assertions.assertThrows(RuntimeException.class, () -> {
			Integer.parseInt("1r");
		});
		e.getMessage();
	}
	/*
	 * @Test public void privateMethodTest() { Method
	 * perivateMehod=(MyProfileDao.class).getDeclaredMethod("doSum");
	 * perivateMehod.setAccessibility(true); int s=(int)perivateMehod.invoke(1,2);
	 * System.out.println(s);
	 * 
	 * }
	 */

}
